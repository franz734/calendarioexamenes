<!DOCTYPE html>
<html lang="en">
@include('admin.header')

<body>
    <div class="wrapper">
        @include('admin.sidebar')
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @yield('contenido')
        </div>
        @include('admin.footer')
</body>

</html>
