<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Calexam extends Model
{
    protected $table = 'calexamw';
    protected $connection = 'sqlsrv';

    static public function getCicloByPtl($ptl){
        $consulta = 'SELECT distinct(ce_ciclo) from dgirew.dbo.calexamw WHERE ce_ptl = ? order by ce_ciclo desc';
        $ciclo = DB::connection('sqlsrv')->select($consulta, [$ptl]);
        foreach($ciclo as $key){
            $key->ce_cicloTxt = "20".substr($key->ce_ciclo,0,2)." - 20".substr($key->ce_ciclo,2,2);
        }
        return $ciclo;
    }
    static public function getPeriodoByPtlCiclo($ptl,$ciclo){
        $consulta = 'SELECT distinct(ce_periodo) from dgirew.dbo.calexamw WHERE ce_ptl = ? AND ce_ciclo = ? order by ce_periodo desc';
        $periodo = DB::connection('sqlsrv')->select($consulta, [$ptl,$ciclo]);
        return $periodo;
    }
    static public function getCalendario($request){
        $ptl = $request->ptl;
        $ciclo = $request->ciclo;
        $periodo = $request->periodo;
        $plan = substr($request->plan,0,2);
        $aplan = substr($request->plan,2,2);
        $vuelta = $request->vuelta;
        $consulta = "SELECT ce_asig, ce_fecha_ex, ce_hora_ex, ce_grupo, ce_expe1 FROM dgirew.dbo.calexamw WHERE
                     ce_ptl = ? AND ce_ciclo=? AND ce_plan=?
                     AND ce_periodo=? AND ce_tipo_ex= 'O'
                     AND ce_vuelta = ? AND ce_aplan = ?";
        $calendario = collect(DB::connection('sqlsrv')->select($consulta, [$ptl,$ciclo,$plan,$periodo,$vuelta,$aplan]));
        return $calendario;
    }
    static public function getGposExamReg($request){
        $ptl = $request->ptl;
        $ciclo = $request->ciclo;
        $periodo = $request->periodo;
        $plan = substr($request->plan,0,2);
        $aplan = substr($request->plan,2,2);
        $vuelta = $request->vuelta;
        $consulta = "SELECT ce_grupo FROM dgirew.dbo.calexamw WHERE
                     ce_ptl = ? AND ce_ciclo=? AND ce_plan=?
                     AND ce_periodo=? AND ce_tipo_ex= 'O'
                     AND ce_vuelta = ? AND ce_aplan = ?";
        $totGposExamReg = collect(DB::connection('sqlsrv')->select($consulta, [$ptl,$ciclo,$plan,$periodo,$vuelta,$aplan]));
        return $totGposExamReg;
    }


}
