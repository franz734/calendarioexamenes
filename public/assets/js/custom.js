var spanish = {
  "sProcessing":     "Procesando...",
  "sLengthMenu":     "Mostrar _MENU_ registros",
  "sZeroRecords":    "No se encontraron resultados",
  "sEmptyTable":     "Ningún dato disponible en esta tabla =(",
  "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
  "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
  "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
  "sInfoPostFix":    "",
  "sSearch":         "Buscar:",
  "sUrl":            "",
  "sInfoThousands":  ",",
  "sLoadingRecords": "Cargando...",
  "oPaginate": {
      "sFirst":    "Primero",
      "sLast":     "Último",
      "sNext":     "Siguiente",
      "sPrevious": "Anterior"
  },
  "oAria": {
      "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
      "sSortDescending": ": Activar para ordenar la columna de manera descendente"
  },
  "buttons": {
      "copy": "Copiar",
      "colvis": "Visibilidad"
  }
}

// Ready //
$(document).ready(function () { 
  var tableCalendario = $('#tableCalendario').DataTable();
    $('.select2').select2({
        placeholder: "Seleccione...",
        allowClear: true
    });
    $('#selectPtl').change(function(){
      $('#selectPeriodo').empty();
      $('#selectPlan').empty();    
      $('#contTable').hide(); 
      $('#contInfo').hide();
      $('#contInfo').empty(); 
      var cvePtl = $(this).val();     
      var cicloArr = [];
      $.ajax({
          url: "../consulta/listaCiclo/" + cvePtl,
          type: 'GET',
          cache: false,
        })
          .done(function (result) {
            //console.log(result)
            //return false;
            for (const i in result) {
              if (result[i] != null) {
                cicloArr.push({
                  id: result[i].ce_ciclo,
                  ciclo: result[i].ce_cicloTxt,
                })
              }
            }
            var cadCom = '<option></option>';
            for (const i in cicloArr) {
              cadCom += '<option value="' + cicloArr[i].id + '">' + cicloArr[i].ciclo
                + '</option>';
            }
            //console.log(cadCom);
            $('#selectCiclo').empty()
            $('#selectCiclo').append(cadCom);
  
          })
          .fail(function (result) {
            console.log(result)
          })
          .always(function () {
            console.log('complete');
          });
    });
    $('#selectCiclo').change(function(){
      var cvePtl = $('#selectPtl').val();
      var ciclo = $(this).val();
      var periodoArr = [];
      $.ajax({
        url: "../consulta/listaPeriodo/"+ cvePtl +'/'+ ciclo,
        type: 'GET',
        cache: false,
      })
        .done(function (result) {
          //console.log(result)
          //return false;
          for (const i in result) {
            if (result[i] != null) {
              periodoArr.push({
                id: result[i].ce_periodo,
                periodo: result[i].ce_periodo,
              })
            }
          }
          var cadCom = '<option></option>';
          for (const i in periodoArr) {
            cadCom += '<option value="' + periodoArr[i].id + '">' + periodoArr[i].periodo
              + '</option>';
          }
          //console.log(cadCom);
          $('#selectPeriodo').empty()
          $('#selectPeriodo').append(cadCom);

        })
        .fail(function (result) {
          console.log(result)
        })
        .always(function () {
          console.log('complete');
        });
    });
    $('#selectPeriodo').change(function(){
      var cvePtl = $('#selectPtl').val();
      var peri = $(this).val();
      var planArr = [];      
      $.ajax({
        url: "../consulta/listaPlan/"+ cvePtl +'/'+ peri,
        type: 'GET',
        cache: false,
      })
        .done(function (result) {
          /* console.log(result)
          return false; */
          for (const i in result) {
            if (result[i] != null) {
              planArr.push({
                id: result[i].pu_plan+result[i].pu_aplan,
                cad: result[i].pu_nombre_plan+' ('+result[i].pu_aplan+')',
              })
            }
          }
          var cadCom = '<option></option>';
          for (const i in planArr) {
            cadCom += '<option value="' + planArr[i].id + '">' + planArr[i].cad
              + '</option>';
          }
          //console.log(cadCom);
          $('#selectPlan').empty()
          $('#selectPlan').append(cadCom);

        })
        .fail(function (result) {
          console.log(result)
        })
        .always(function () {
          console.log('complete');
        });
    });
    //buscar calendario//
    $('#buscaCalendario').click(function(e){
      e.preventDefault();
      var campos = $('#filterForm').find('.validar').parsley();    
      var ctl = 0;
      for (const i in campos) {
        if (campos[i].isValid()) {
          ctl++
        }
        else {
          campos[i].validate();
          
        }
      }        
      if(campos.length == ctl){
        var formdata = new FormData($("#filterForm")[0]);
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      Swal.fire({
        title: 'Buscando...',
        allowEscapeKey: false,
        allowOutsideClick: false,
      })
      Swal.showLoading();
      $.ajax({
        url: "../consultas/getCalendario",
        type: 'POST',
        dataType: 'json',
        data: formdata,
        cache: false,
        processData: false,
        contentType: false,
      })
        .done(function (result) {         
          //console.log(result)
          //return false;
          //var data = JSON.parse(result.lista);
          Swal.close();
          var data = result.lista;
          var info = result.info;
          //console.log(data);
          //return false;
          $('#contInfo').show();
          var cad = '<div class="card-body">'+
                    '<h5 class="text-center">'+info.infoPtl.ptl_ptl+' '+info.infoPtl.ptl_nombre+'</h5>'+
                    '<p class="text-center">'+info.infoPlan.pu_plan+' - '+info.infoPlan.pu_nombre_plan+' ('+info.infoPlan.pu_aplan+')</p>'+
                    '<p class="text-center"> Total de Grupos Inscritos: '+info.infoGpos.totGposIns+'</p>'+
                    '<p class="text-center"> Total de Grupos con Examenes Registrados: '+info.infoGpos.totGposExamReg+'</p>'+
                    '<p class="text-center"> Total de Grupos Pendientes por Registrar Examen: '+info.infoGpos.totGposPendReg+'</p>'+
                    '</div>';          
          $('#contInfo').html(cad);
          $('#contTable').show();
          $('#tableCalendario').DataTable().destroy();
          var tableCalendario = $('#tableCalendario').DataTable({   
            "language":spanish,         
            "autoWidth": false,
            "responsive": true,
            "data":data.data,
            "columns": [
              {"data":"num"},
              {"data":"ce_asig"},
              {"data": "infoAsig.pa_nombre"},              
              {"data":"ce_grupo"},
              {"data":"ce_fecha_ex"},
              {"data":"ce_hora_ex"},
              {"data":"ce_expe1"},
              {"data":"infoProf.prof_nombre"},
            ],
          });
          
        })
        .fail(function (result) {
          console.log('Fail');          
          //alert(result.responseJSON)
        })
        .always(function () {
          console.log('complete');
        });
      }
      
    });
    $('#limpiaConsulta').click(function(e){
      e.preventDefault();
      $('.sel2').val(null).trigger('change');;
      $('#contTable').hide(); 
      $('#tableCalendario').DataTable().destroy();
      $('#contInfo').hide();
      $('#contInfo').empty(); 

    })
});