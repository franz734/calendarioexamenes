@extends('admin.master')
@section('css')
    
@endsection
@section('contenido')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Consulta Calendario de Exámenes</h1>
                </div>
            </div>
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card card-primary card-outline">                        
                <div class="card-body">
                    <form id="filterForm">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Institución</label>
                                    <select id="selectPtl" name="ptl" class="form-control select2 validar sel2" data-parsley-errors-container="#errPtl" required>    
                                        <option></option>                                
                                        @foreach ($planteles as $plantel)
                                            <option value="{{$plantel->ptl_ptl}}">{{$plantel->ptl_ptl}} {{utf8_encode($plantel->ptl_nombre)}}</option>
                                        @endforeach
                                    </select>
                                    <div id="errPtl" class="errorBlock"></div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Ciclo</label>
                                    <select id="selectCiclo" name="ciclo" class="form-control select2 validar sel2" style="width: 100%;" data-parsley-errors-container="#errCiclo" required>                                                                                
                                    </select>
                                    <div id="errCiclo" class="errorBlock"></div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Periodo</label>
                                    <select id="selectPeriodo" name="periodo" class="form-control select2 validar sel2" style="width: 100%;" data-parsley-errors-container="#errPeri" required>    
                                    </select>
                                    <div id="errPeri" class="errorBlock"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Plan</label>
                                    <select id="selectPlan" name="plan" class="form-control select2 validar sel2" style="width: 100%;" data-parsley-errors-container="#errPlan" required>                                        
                                    </select>
                                    <div id="errPlan" class="errorBlock"></div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Vuelta</label>
                                    <select id="selectVuelta" name="vuelta" class="form-control select2 validar sel2" style="width: 100%;" data-parsley-errors-container="#errVuelta" required>
                                        <option></option>
                                        <option value="1">1ra Vuelta</option>
                                        <option value="2">2da Vuelta</option>
                                    </select>
                                    <div id="errVuelta" class="errorBlock"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 offset-md-10">
                                <a id="limpiaConsulta" class="btn btn-primary miBtn">Limpiar</a>
                                <a id="buscaCalendario" class="btn btn-primary miBtn">Buscar</a>
                            </div>
                        </div>
                    </form>
                </div><!-- /.card-body -->                  
            </div>
            {{-- Info --}}            
            <div class="row">
                <div class="card card-primary card-outline col-md-6 offset-md-3" id="contInfo" style="display:none">                
                </div>
            </div>
            {{-- Datatable --}}
            <div class="card card-primary" id="contTable" style="display:none">                        
                <div class="card-body">
                    <div>
                        <table class="table table-bordered table-striped" id="tableCalendario" style="width:100%">
                            <thead class="bg-primary">
                                <tr>                            
                                    <th style="width: 10px;">Num.</th>
                                    <th style="width: 10px;">Clave</th>
                                    <th style="width: 250px;">Nombre</th>
                                    <th style="width: 10px;">Grupo</th>
                                    <th style="width: 12px;">Fecha</th>
                                    <th style="width: 10px;">Hora</th>
                                    <th style="width: 12px;">Expediente</th>
                                    <th>Profesor</th>
                                </tr>
                            </thead>
                        </table>
                    </div> 
                </div>
            </div>             
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

@endsection
@section('scripts')
    
@endsection