<?php
///CONSULTAS///
/** UMANSIW **/
function getPlanteles(){
    $consulta = "SELECT ptl_ptl,ptl_nombre from unamsiw.dbo.planteles where ptl_vig='S' order by ptl_nombre asc";
    //dd($consulta);
    $planteles  = collect(DB::connection('sqlsrvUNAMSIW')->select($consulta));
    return $planteles;
}
function getPlanes($ptl,$ciclo){
    //$consulta = "SELECT pli_plan, pli_aplan FROM unamsiw.dbo.planinco WHERE pli_ptl = ? and pli_ciclo = ?";
    $consulta = "SELECT 
    distinct pli_plan
    ,pli_aplan
    ,pu_nombre_plan 
    ,pu_plan
    ,pu_aplan
    ,pu_duracion
    ,pu_fac
    ,pu_anualsem
    ,pu_grado
    ,pu_seriacion
    FROM unamsi.dbo.planinco,unamsi.dbo.planunam 
    WHERE pli_ptl='$ptl' and pli_ciclo='$ciclo'  
    and pli_plan=pu_plan and pli_aplan=pu_aplan 
    order by pli_plan";
    $planes = collect(DB::connection('sqlsrvUNAMSI')->select($consulta,[$ptl,$ciclo]));
    return $planes;
}
function getNomPlan($plan, $aplan){
    $consulta = "SELECT distinct (pu_nombre_plan),pu_anualsem,pu_plan,pu_aplan from unamsiw.dbo.planunam where pu_plan= ? and pu_aplan = ?";
    $infoPlan = DB::connection('sqlsrvUNAMSIW')->select($consulta,[$plan,$aplan]);    
    return $infoPlan;
}
function getNomPtl($ptl){
    $consulta = "SELECT ptl_nombre,ptl_ptl from unamsiw.dbo.planteles where ptl_vig='S' and ptl_ptl = ? order by ptl_nombre asc";
    //dd($consulta);
    $nomPtl = DB::connection('sqlsrvUNAMSIW')->select($consulta,[$ptl]);
    return $nomPtl;
}
function getNomAsig($plan,$aplan,$asig){
    $consulta = "SELECT pa_nombre from unamsiw.dbo.planasig where pa_plan=? 
                 and pa_aplan=? and pa_asig=?";
    $nomAsig = DB::connection('sqlsrvUNAMSIW')->select($consulta,[$plan,$aplan,$asig]);
    return $nomAsig;
}
function getNomProf($expe){
    $consulta = "SELECT prof_nombre from unamsiw.dbo.profesores where prof_exp=?";
    $nomProf = DB::connection('sqlsrvUNAMSIW')->select($consulta,[$expe]);
    return $nomProf;
}
/** DGIREW **/

/** UNAMSI **/
function getTotGposIns($periodo,$ptl,$plan,$aplan){
    $consulta = "SELECT count(*) as total 
                 from unamsi3.dbo.w_grupos_w 
                 where g_periodo=? and g_tipoexam='O' 
                 and g_ptl=? and g_plan=? 
                 and g_aplan=?";                 
    $totGpos = DB::connection('sqlsrvUNAMSI')->select($consulta,[$periodo,$ptl,$plan,$aplan]);
    return array_pop($totGpos);
}



?>