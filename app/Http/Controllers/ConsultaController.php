<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Carbon\Carbon;
use Datatables;
use App\Calexam;


class ConsultaController extends Controller
{
    /// Pnael de Inicio ///
    public function inicio(){
        return view('admin.panel');
    }
    /// Panel para ingresar parametros de busqueda ///
    public function consultar(){
        $planteles = getPlanteles();        
        return view('admin.consultar',['planteles'=>$planteles]);
    }
    public function getCiclo($ptl){
        $ciclos = Calexam::getCicloByPtl($ptl);
        return response()->json($ciclos,200);
    }
    public function getPeriodo($ptl, $ciclo){        
        $periodos = Calexam::getPeriodoByPtlCiclo($ptl, $ciclo);
        return response()->json($periodos,200);
    }
    public function getPlan($ptl, $peri){ 
        $peri = substr($peri,0,2);
        $planes = getPlanes($ptl, $peri);
        foreach($planes as $plan){            
            /* $infoPlan = getNomPlan($plan->pli_plan, $plan->pli_aplan);            
            $plan->info = (object)array_pop($infoPlan); */
            $plan->pu_nombre_plan = utf8_encode($plan->pu_nombre_plan);
        }
        //dd($planes);
        return response()->json($planes,200);
    }
    public function getCalendario(Request $request){
        //dd($request->all());
        $response = [];
        $num = 1;
        $plan = substr($request->plan,0,2);
        $aplan = substr($request->plan,2,2);
        $calendario = Calexam::getCalendario($request);
        $infoPtl = getNomPtl($request->ptl);                        
        $infoPtl = (object)array_pop($infoPtl); 
        $infoPtl->ptl_nombre = utf8_encode($infoPtl->ptl_nombre);
        $infoPlan = getNomPlan($plan,$aplan);
        $infoPlan = (object)array_pop($infoPlan);         
        $infoPlan->pu_nombre_plan = utf8_encode($infoPlan->pu_nombre_plan);
        $totGpos = getTotGposIns($request->periodo,$request->ptl,$plan,$aplan);
        $totGpos = $totGpos->total;
        $totGposExamReg = Calexam::getGposExamReg($request);
        $totGposExamReg = $totGposExamReg->count();       
        $infoGpos = (object)[
            'totGposIns'=>$totGpos,
            'totGposExamReg'=>$totGposExamReg,
            'totGposPendReg'=>(int)$totGpos-(int)$totGposExamReg
        ];        
        foreach($calendario as $key){
            $key->num = $num;
            $infoAsig = getNomAsig($plan,$aplan,$key->ce_asig);
            $key->infoAsig = (object)array_pop($infoAsig);
            $key->infoAsig->pa_nombre = utf8_encode($key->infoAsig->pa_nombre);
            $infoProf = getNomProf($key->ce_expe1);
            $key->infoProf = (object)array_pop($infoProf);
            $key->infoProf->prof_nombre = trim(utf8_encode($key->infoProf->prof_nombre)); 
            $key->ce_fecha_ex = Carbon::createFromFormat('Y-m-d H:i:s',$key->ce_fecha_ex)->format('d/m/Y');
            $key->ce_hora_ex = Carbon::parse($key->ce_hora_ex)->format('H:i');
            $num++;         
        }                
        //dd($infoPtl);
        $calendarioObj = [            
            'infoPtl'=>$infoPtl, 
            'infoPlan'=>$infoPlan,
            'infoGpos'=>$infoGpos
        ];             
        $calendario = Datatables::of($calendario)->make(true);
        //dd($calendario);
        $response = [
            'lista'=>json_decode($calendario->content()),
            'info'=>$calendarioObj
        ];   
        //return $calendario;
        //dd($response);
        return response()->json($response,200);
        
       
    }
}
