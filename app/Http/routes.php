<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/* Route::get('/', function () {      
    return view('admin.panel');
}); */
/////////////////////
///** Cnsulta **////
//////////////////
///GET///
Route::get('/consulta/inicio', 'ConsultaController@inicio');
Route::get('/consulta/consultar', 'ConsultaController@consultar');
Route::get('/consulta/listaCiclo/{ptl}','ConsultaController@getCiclo');
Route::get('/consulta/listaPeriodo/{ptl}/{ciclo}','ConsultaController@getPeriodo');
Route::get('/consulta/listaPlan/{ptl}/{peri}','ConsultaController@getPlan');

///POST///
Route::post('/consultas/getCalendario', 'ConsultaController@getCalendario');